# Audit logging
## Over
Eén van de zaken die door GDPR wordt opgelegd, is audit logging.  Er moet bijgehouden welke gebruiker van een applicatie persoonlijke gegevens van iemand anders te zien gekregen heeft.

## Consistent formaat
Er wordt ons niet opgelegd in welk formaat we dit moeten loggen, maar het moet wel gestructureerd zijn.  We kiezen er dan ook voor om het formaat van de audit log records consistent te maken over de verschillende applicaties heen.

Deze library biedt je de mogelijkheid om de audit log record op te bouwen en om te vormen naar een formaat dat dan consistent door de andere applicaties gebruikt kan worden.  Momenteel bieden we een `com.google.gson.JsonObject` aan; op termijn kan dit gewijzigd of aangevuld worden.

## Dependencies
We leggen bewust geen dependency naar een extern systeem; het staat de applicaties vrij om te kiezen naar waar ze de log records wegschrijven.

## Codevoorbeeld

```java
    public void logAuditGegevens(...) {
        Applicatie applicatie = new Applicatie("de naam", "de versie");
        Gebruiker gebruiker = new Gebruiker("de user id");

        BekekenPersoon bekekenPersoon = new BekekenPersoon();
        bekekenPersoon.setNaam("naam");
        bekekenPersoon.setVoornaam("voornaam");
        bekekenPersoon.setInsz("INSZ");
        bekekenPersoon.setGeboortedatum(geboortedatum);

        AuditLogRecord auditLogRecord = new AuditLogRecord.Builder()
                .withApplicatie(applicatie)
                .withGebruiker(gebruiker)
                .withBekekenScherm("een schermidentificatie")
                .withBekekenPersoon(bekekenPersoon)
                .build();
        
        JsonObject jsonObject = auditLogRecord.toJsonObject();
        // indien gewenst, kan je hier nog iets extra toevoegen aan het jsonObject

        LOGGER.info(jsonObject.toString());
    }
```