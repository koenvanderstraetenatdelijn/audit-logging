package be.delijn.auditlogging;

import com.google.gson.JsonObject;
import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ISODateTimeFormatter.class)
public class BekekenPersoonTest {
    private static final String NAAM = "een naam";
    private static final String VOORNAAM = "een voornaam";
    private static final String INSZ = "een insz";

    private static final LocalDate GEBOORTEDATUM = new LocalDate();
    private static final String GEFORMATTEERDE_GEBOORTEDATUM = "geformatteerde geboortedatum";

    @Test
    public void toJsonObject() {
        PowerMockito.mockStatic(ISODateTimeFormatter.class);
        PowerMockito.when(ISODateTimeFormatter.printAsISODate(GEBOORTEDATUM)).thenReturn(GEFORMATTEERDE_GEBOORTEDATUM);

        BekekenPersoon bekekenPersoon = new BekekenPersoon();
        bekekenPersoon.setNaam(NAAM);
        bekekenPersoon.setVoornaam(VOORNAAM);
        bekekenPersoon.setInsz(INSZ);
        bekekenPersoon.setGeboortedatum(GEBOORTEDATUM);

        JsonObject actualJsonObject = bekekenPersoon.toJsonObject();
        assertThat(actualJsonObject.get("naam").getAsString()).isEqualTo(NAAM);
        assertThat(actualJsonObject.get("voornaam").getAsString()).isEqualTo(VOORNAAM);
        assertThat(actualJsonObject.get("insz").getAsString()).isEqualTo(INSZ);
        assertThat(actualJsonObject.get("geboortedatum").getAsString()).isEqualTo(GEFORMATTEERDE_GEBOORTEDATUM);
    }
}