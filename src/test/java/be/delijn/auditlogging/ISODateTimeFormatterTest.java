package be.delijn.auditlogging;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ISODateTimeFormatterTest {

    @Test
    public void printAsISODateTimeSlaagtBijValueNull() {
        String formattedDateTime = ISODateTimeFormatter.printAsISODateTime(null);
        assertThat(formattedDateTime).isNull();
    }

    @Test
    public void printAsISODateTimeSlaagtBijValueNietNull() {
        DateTime dateTimeToFormat = new DateTime(2018, 8, 7, 10, 38, 24, 123, DateTimeZone.forOffsetHours(4));
        String formattedDateTime = ISODateTimeFormatter.printAsISODateTime(dateTimeToFormat);
        assertThat(formattedDateTime).isEqualTo("2018-08-07T10:38:24.123+04:00");
    }

    @Test
    public void printAsISODateSlaagtBijValueNull() {
        String formattedLocalDate = ISODateTimeFormatter.printAsISODate(null);
        assertThat(formattedLocalDate).isNull();
    }

    @Test
    public void printAsISODateSlaagtBijValueNietNull() {
        LocalDate localDateToFormat = new LocalDate(2018, 8, 7);
        String formattedLocalDate = ISODateTimeFormatter.printAsISODate(localDateToFormat);
        assertThat(formattedLocalDate).isEqualTo("2018-08-07");
    }


}