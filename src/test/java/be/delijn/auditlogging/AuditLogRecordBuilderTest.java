package be.delijn.auditlogging;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DateTime.class)
public class AuditLogRecordBuilderTest {

    private static final DateTime TIJDSTIP_NU = DateTime.now();
    private static final DateTime TIJDSTIP_GISTEREN = DateTime.now().minusDays(1);

    private static final String BEKEKEN_SCHERM = "een scherm";

    @Mock
    private Applicatie applicatie;

    @Mock
    private Gebruiker gebruiker;

    @Mock
    private BekekenPersoon bekekenPersoon;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(DateTime.class);
        PowerMockito.when(DateTime.now()).thenReturn(TIJDSTIP_NU);
    }

    @Test
    public void buildFaaltWanneerApplicatieNullIs() {
        try {
            new AuditLogRecord.Builder()
                    .withApplicatie(null)
                    .withTijdstip(TIJDSTIP_NU)
                    .withGebruiker(gebruiker)
                    .withBekekenScherm(BEKEKEN_SCHERM)
                    .withBekekenPersoon(bekekenPersoon)
                    .build();
        } catch (NullPointerException e) {
            assertThat(e.getMessage()).isEqualTo("Applicatie is verplicht.");
        }
    }

    @Test
    public void buildFaaltWanneerTijdstipNullIs() {
        try {
            new AuditLogRecord.Builder()
                    .withApplicatie(applicatie)
                    .withTijdstip(null)
                    .withGebruiker(gebruiker)
                    .withBekekenScherm(BEKEKEN_SCHERM)
                    .withBekekenPersoon(bekekenPersoon)
                    .build();
        } catch (NullPointerException e) {
            assertThat(e.getMessage()).isEqualTo("Tijdstip is verplicht.");
        }
    }

    @Test
    public void buildFaaltWanneerGebruikerNullIs() {
        try {
            new AuditLogRecord.Builder()
                    .withApplicatie(applicatie)
                    .withTijdstip(TIJDSTIP_NU)
                    .withGebruiker(null)
                    .withBekekenScherm(BEKEKEN_SCHERM)
                    .withBekekenPersoon(bekekenPersoon)
                    .build();
        } catch (NullPointerException e) {
            assertThat(e.getMessage()).isEqualTo("Gebruiker is verplicht.");
        }
    }

    @Test
    public void buildFaaltWanneerBekekenSchermNullIs() {
        try {
            new AuditLogRecord.Builder()
                    .withApplicatie(applicatie)
                    .withTijdstip(TIJDSTIP_NU)
                    .withGebruiker(gebruiker)
                    .withBekekenScherm(null)
                    .withBekekenPersoon(bekekenPersoon)
                    .build();
        } catch (NullPointerException e) {
            assertThat(e.getMessage()).isEqualTo("Bekeken scherm is verplicht.");
        }
    }

    @Test
    public void buildFaaltWanneerBekekenPersoonNullIs() {
        try {
            new AuditLogRecord.Builder()
                    .withApplicatie(applicatie)
                    .withTijdstip(TIJDSTIP_NU)
                    .withGebruiker(gebruiker)
                    .withBekekenScherm(BEKEKEN_SCHERM)
                    .withBekekenPersoon(null)
                    .build();
        } catch (NullPointerException e) {
            assertThat(e.getMessage()).isEqualTo("Bekeken persoon is verplicht.");
        }
    }

    @Test
    public void buildSlaagtMetDefaultTijdstip() {
        AuditLogRecord auditLogRecord = new AuditLogRecord.Builder()
                .withApplicatie(applicatie)
                .withGebruiker(gebruiker)
                .withBekekenScherm(BEKEKEN_SCHERM)
                .withBekekenPersoon(bekekenPersoon)
                .build();

        assertThat(auditLogRecord.getTijdstip()).isEqualTo(TIJDSTIP_NU);
    }

    @Test
    public void buildSlaagtWanneerTijdstipMeegegevenWordt() {
        AuditLogRecord auditLogRecord = new AuditLogRecord.Builder()
                .withApplicatie(applicatie)
                .withTijdstip(TIJDSTIP_GISTEREN)
                .withGebruiker(gebruiker)
                .withBekekenScherm(BEKEKEN_SCHERM)
                .withBekekenPersoon(bekekenPersoon)
                .build();

        assertThat(auditLogRecord.getTijdstip()).isEqualTo(TIJDSTIP_GISTEREN);
    }
}
