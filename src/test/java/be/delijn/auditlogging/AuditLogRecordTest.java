package be.delijn.auditlogging;

import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ISODateTimeFormatter.class)
public class AuditLogRecordTest {
    private static final DateTime TIJDSTIP = DateTime.now();
    private static final String GEFORMATTEERD_TIJDSTIP = "geformatteerd tijdstip nu";
    private static final String BEKEKEN_SCHERM = "een scherm";

    @Mock
    private Applicatie applicatie;

    @Mock
    private Gebruiker gebruiker;

    @Mock
    private BekekenPersoon bekekenPersoon;

    private JsonObject applicatieJsonObject;
    private JsonObject gebruikerJsonObject;
    private JsonObject bekekenPersoonJsonObject;

    @Before
    public void setUp() {
        PowerMockito.mockStatic(ISODateTimeFormatter.class);
        PowerMockito.when(ISODateTimeFormatter.printAsISODateTime(TIJDSTIP)).thenReturn(GEFORMATTEERD_TIJDSTIP);

        applicatieJsonObject = new JsonObject();
        applicatieJsonObject.addProperty("applicatie", "dummy applicatie");

        gebruikerJsonObject = new JsonObject();
        gebruikerJsonObject.addProperty("gebruiker", "dummy gebruiker");

        bekekenPersoonJsonObject = new JsonObject();
        bekekenPersoonJsonObject.addProperty("bekekenPersoon", "dummy persoon");
    }

    @Test
    public void toJsonObject() {
        when(applicatie.toJsonObject()).thenReturn(applicatieJsonObject);
        when(gebruiker.toJsonObject()).thenReturn(gebruikerJsonObject);
        when(bekekenPersoon.toJsonObject()).thenReturn(bekekenPersoonJsonObject);

        AuditLogRecord auditLogRecord = new AuditLogRecord.Builder()
                .withApplicatie(applicatie)
                .withTijdstip(TIJDSTIP)
                .withGebruiker(gebruiker)
                .withBekekenScherm(BEKEKEN_SCHERM)
                .withBekekenPersoon(bekekenPersoon)
                .build();

        JsonObject actualJsonObject = auditLogRecord.toJsonObject();
        assertThat(actualJsonObject.getAsJsonObject("applicatie")).isEqualTo(applicatieJsonObject);
        assertThat(actualJsonObject.getAsJsonObject("gebruiker")).isEqualTo(gebruikerJsonObject);
        assertThat(actualJsonObject.getAsJsonObject("bekekenPersoon")).isEqualTo(bekekenPersoonJsonObject);
        assertThat(actualJsonObject.get("tijdstip").getAsString()).isEqualTo(GEFORMATTEERD_TIJDSTIP);
        assertThat(actualJsonObject.get("bekekenScherm").getAsString()).isEqualTo(BEKEKEN_SCHERM);
    }
}
