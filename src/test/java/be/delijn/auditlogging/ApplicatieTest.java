package be.delijn.auditlogging;

import com.google.gson.JsonObject;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ApplicatieTest {
    private static final String NAAM = "een naam";
    private static final String VERSIE = "een versie";

    @Test
    public void toJsonObjectMetNaam() {
        Applicatie applicatie = new Applicatie(NAAM);

        JsonObject actualJsonObject = applicatie.toJsonObject();
        assertThat(actualJsonObject.get("naam").getAsString()).isEqualTo(NAAM);
        assertThat(actualJsonObject.get("versie")).isNull();
    }

    @Test
    public void toJsonObjectMetNaamEnVersie() {
        Applicatie applicatie = new Applicatie(NAAM, VERSIE);

        JsonObject actualJsonObject = applicatie.toJsonObject();
        assertThat(actualJsonObject.get("naam").getAsString()).isEqualTo(NAAM);
        assertThat(actualJsonObject.get("versie").getAsString()).isEqualTo(VERSIE);
    }
}