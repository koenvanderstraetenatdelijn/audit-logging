package be.delijn.auditlogging;

import com.google.gson.JsonObject;
import org.apache.commons.lang3.Validate;
import org.joda.time.DateTime;

public class AuditLogRecord {
    private Applicatie applicatie;
    private DateTime tijdstip;

    private Gebruiker gebruiker;
    private String bekekenScherm;
    private BekekenPersoon bekekenPersoon;

    public Applicatie getApplicatie() {
        return applicatie;
    }

    public DateTime getTijdstip() {
        return tijdstip;
    }

    public Gebruiker getGebruiker() {
        return gebruiker;
    }

    public String getBekekenScherm() {
        return bekekenScherm;
    }

    public BekekenPersoon getBekekenPersoon() {
        return bekekenPersoon;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("applicatie", applicatie.toJsonObject());
        jsonObject.addProperty("tijdstip", ISODateTimeFormatter.printAsISODateTime(this.tijdstip));
        jsonObject.add("gebruiker", gebruiker.toJsonObject());
        jsonObject.addProperty("bekekenScherm", bekekenScherm);
        jsonObject.add("bekekenPersoon", bekekenPersoon.toJsonObject());

        return jsonObject;
    }

    public static class Builder {
        private Applicatie applicatie;
        private DateTime tijdstip;

        private Gebruiker gebruiker;
        private String bekekenScherm;
        private BekekenPersoon bekekenPersoon;

        public Builder() {
            tijdstip = DateTime.now();
        }

        public Builder withApplicatie(Applicatie applicatie) {
            this.applicatie = applicatie;
            return this;
        }

        public Builder withTijdstip(DateTime tijdstip) {
            this.tijdstip = tijdstip;
            return this;
        }

        public Builder withGebruiker(Gebruiker gebruiker) {
            this.gebruiker = gebruiker;
            return this;
        }

        public Builder withBekekenScherm(String bekekenScherm) {
            this.bekekenScherm = bekekenScherm;
            return this;
        }

        public Builder withBekekenPersoon(BekekenPersoon bekekenPersoon) {
            this.bekekenPersoon = bekekenPersoon;
            return this;
        }

        public AuditLogRecord build() {
            Validate.notNull(applicatie, "Applicatie is verplicht.");
            Validate.notNull(tijdstip, "Tijdstip is verplicht.");
            Validate.notNull(gebruiker, "Gebruiker is verplicht.");
            Validate.notNull(bekekenScherm, "Bekeken scherm is verplicht.");
            Validate.notNull(bekekenPersoon, "Bekeken persoon is verplicht.");

            AuditLogRecord auditLogRecord = new AuditLogRecord();
            auditLogRecord.applicatie = applicatie;
            auditLogRecord.tijdstip = tijdstip;
            auditLogRecord.gebruiker = gebruiker;
            auditLogRecord.bekekenScherm = bekekenScherm;
            auditLogRecord.bekekenPersoon = bekekenPersoon;

            return auditLogRecord;
        }
    }
}
