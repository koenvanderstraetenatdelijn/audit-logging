package be.delijn.auditlogging;

import com.google.gson.JsonObject;

public class Gebruiker {
    private String userId;

    public Gebruiker(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userId", userId);

        return jsonObject;
    }
}
