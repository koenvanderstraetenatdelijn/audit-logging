package be.delijn.auditlogging;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.ISODateTimeFormat;

class ISODateTimeFormatter {

    private ISODateTimeFormatter() {
        // Utility class
    }

    static String printAsISODateTime(DateTime dateTimeToFormat) {
        if (dateTimeToFormat == null) {
            return null;
        }

        return ISODateTimeFormat.dateTime().print(dateTimeToFormat);
    }

    static String printAsISODate(LocalDate localDateToFormat) {
        if (localDateToFormat == null) {
            return null;
        }

        return ISODateTimeFormat.date().print(localDateToFormat);
    }

}