package be.delijn.auditlogging;

import com.google.gson.JsonObject;
import org.joda.time.LocalDate;

public class BekekenPersoon {
    private String naam;
    private String voornaam;
    private String insz;
    private LocalDate geboortedatum;

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getInsz() {
        return insz;
    }

    public void setInsz(String insz) {
        this.insz = insz;
    }

    public LocalDate getGeboortedatum() {
        return geboortedatum;
    }

    public void setGeboortedatum(LocalDate geboortedatum) {
        this.geboortedatum = geboortedatum;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("naam", naam);
        jsonObject.addProperty("voornaam", voornaam);
        jsonObject.addProperty("insz", insz);
        jsonObject.addProperty("geboortedatum", ISODateTimeFormatter.printAsISODate(geboortedatum));

        return jsonObject;
    }
}
