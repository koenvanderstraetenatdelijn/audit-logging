package be.delijn.auditlogging;

import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;

public class Applicatie {
    private String naam;
    private String versie;

    public Applicatie(String naam) {
        this.naam = naam;
    }

    public Applicatie(String naam, String versie) {
        this(naam);
        this.versie = versie;
    }

    public String getNaam() {
        return naam;
    }

    public String getVersie() {
        return versie;
    }

    public JsonObject toJsonObject() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("naam", naam);

        if (StringUtils.isNotBlank(versie)) {
            jsonObject.addProperty("versie", versie);
        }

        return jsonObject;
    }
}
